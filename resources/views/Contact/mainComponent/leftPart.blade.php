 <div class="col-lg-8">
     <div class="p-a30 bg-gray clearfix m-b30 ">
         <h2>Send Message Us</h2>
         <div class="dzFormMsg"></div>
         <form method="post" class="dzForm" action="script/contact.php">
             <input type="hidden" value="Contact" name="dzToDo">
             <div class="row">
                 <div class="col-md-12">
                     <div class="form-group">
                         <div class="input-group">
                             <h1>Hi, I am {{ session('name') }}</h1>
                         </div>
                     </div>
                 </div>

                 <div class="col-md-12">
                     <div class="form-group">
                         <div class="input-group">
                             <h1>Hi, I am {{ session('account') }}</h1>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-6">
                     <div class="form-group">
                         <div class="input-group">
                             <input name="dzName" type="text" required class="form-control" placeholder="Your Name">
                         </div>
                     </div>
                 </div>
                 <div class="col-md-6">
                     <div class="form-group">
                         <div class="input-group">
                             <input name="dzEmail" type="email" class="form-control" required
                                 placeholder="Your Email Id">
                         </div>
                     </div>
                 </div>
                 <div class="col-md-6">
                     <div class="form-group">
                         <div class="input-group">
                             <input name="dzOther[Phone]" type="text" required class="form-control"
                                 placeholder="Phone">
                         </div>
                     </div>
                 </div>
                 <div class="col-md-6">
                     <div class="form-group">
                         <div class="input-group">
                             <input name="dzOther[Subject]" type="text" required class="form-control"
                                 placeholder="Subject">
                         </div>
                     </div>
                 </div>
                 <div class="col-md-12">
                     <div class="form-group">
                         <div class="input-group">
                             <textarea name="dzMessage" rows="4" class="form-control" required
                                 placeholder="Your Message..."></textarea>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-12">
                     <div class="form-group">
                         <div class="input-group">
                             <div class="g-recaptcha" data-sitekey="<!-- Put reCaptcha Site Key -->"
                                 data-callback="verifyRecaptchaCallback"
                                 data-expired-callback="expiredRecaptchaCallback"></div>
                             <input class="form-control d-none" style="display:none;" data-recaptcha="true" required
                                 data-error="Please complete the Captcha">
                         </div>
                     </div>
                 </div>
                 <div class="col-md-12">
                     <button name="submit" type="submit" value="Submit" class="site-button ">
                         <span>Submit</span> </button>
                 </div>
             </div>
         </form>
     </div>
 </div>
