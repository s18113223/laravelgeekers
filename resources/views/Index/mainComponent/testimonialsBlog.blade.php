 <div class="section-full overlay-black-middle bg-img-fix content-inner-1"
     style="background-image:url(images/background/bg2.jpg);">
     <div class="container">
         <div class="section-head text-white text-center">
             <h2 class="text-uppercase">What peolpe are saying</h2>
             <div class="dez-separator-outer ">
                 <div class="dez-separator bg-white  style-skew"></div>
             </div>
         </div>
         <div class="section-content">
             <div class="testimonial-four owl-theme owl-carousel">
                 <div class="item">
                     <div class="testimonial-4 testimonial-bg">
                         <div class="testimonial-pic"><img src="images/testimonials/pic1.jpg" width="100" height="100"
                                 alt=""></div>
                         <div class="testimonial-text">
                             <p>The entire team is extremely creative and forward thinking. They are also
                                 very quick and efficient when executing changes for us.</p>
                         </div>
                         <div class="testimonial-detail"> <strong class="testimonial-name">Marina
                                 Lee</strong> <span class="testimonial-position">Media Specialist</span>
                         </div>
                         <div class="quote-right"></div>
                     </div>
                 </div>
                 <div class="item">
                     <div class="testimonial-4 testimonial-bg">
                         <div class="testimonial-pic"><img src="images/testimonials/pic1.jpg" width="100" height="100"
                                 alt=""></div>
                         <div class="testimonial-text">
                             <p>I found their expertise to be extremely helpful. I think it is awesome and I
                                 can't thank you enough for working so closely.</p>
                         </div>
                         <div class="testimonial-detail"> <strong class="testimonial-name">Eloise
                                 Carson</strong> <span class="testimonial-position">Senior Specialist</span>
                         </div>
                         <div class="quote-right"></div>
                     </div>
                 </div>
                 <div class="item">
                     <div class="testimonial-4 testimonial-bg">
                         <div class="testimonial-pic"><img src="images/testimonials/pic1.jpg" width="100" height="100"
                                 alt=""></div>
                         <div class="testimonial-text">
                             <p>The entire team is extremely creative and forward thinking. They are also
                                 very quick and efficient when executing changes for us.</p>
                         </div>
                         <div class="testimonial-detail"> <strong class="testimonial-name">David
                                 Matin</strong> <span class="testimonial-position">Media Specialist</span>
                         </div>
                         <div class="quote-right"></div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
