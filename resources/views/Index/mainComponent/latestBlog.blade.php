<div class="section-full content-inner ">
    <div class="container">
        <div class="section-head text-center">
            <h2 class="text-uppercase">Latest blog post</h2>
            <div class="dez-separator-outer ">
                <div class="dez-separator bg-secondry style-skew"></div>
            </div>
        </div>
        <div class="section-content ">
            <div class="blog-carousel owl-carousel owl-none">
                <div class="item">
                    <div class="blog-post latest-blog-1 date-style-3 skew-date">
                        <div class="dez-post-media dez-img-effect zoom-slow"> <a href="javascript:;"><img
                                    src="images/blog/latest-blog/pic1.jpg" alt=""></a> </div>
                        <div class="dez-post-info p-t10">
                            <div class="dez-post-title ">
                                <h3 class="post-title"><a href="javascript:;">Construction Planning</a>
                                </h3>
                            </div>
                            <div class="dez-post-meta ">
                                <ul>
                                    <li class="post-date"> <i class="fa fa-calendar"></i><strong>10
                                            Aug</strong> <span> 2020</span> </li>
                                    <li class="post-author"><i class="fa fa-user"></i>By <a
                                            href="javascript:;">demongo</a> </li>
                                    <li class="post-comment"><i class="fa fa-comments"></i> <a href="javascript:;">0
                                            comment</a> </li>
                                </ul>
                            </div>
                            <div class="dez-post-text">
                                <p>This is our latest project which is perfectly constructed. We provide
                                    high-end residential construction, hospitals, hotels...</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="blog-post latest-blog-1 date-style-3 skew-date">
                        <div class="dez-post-media dez-img-effect zoom-slow"> <a href="javascript:;"><img
                                    src="images/blog/latest-blog/pic2.jpg" alt=""></a> </div>
                        <div class="dez-post-info p-t10">
                            <div class="dez-post-title ">
                                <h3 class="post-title"><a href="javascript:;">Professional Services</a>
                                </h3>
                            </div>
                            <div class="dez-post-meta ">
                                <ul>
                                    <li class="post-date"> <i class="fa fa-calendar"></i><strong>10
                                            Aug</strong> <span> 2020</span> </li>
                                    <li class="post-author"><i class="fa fa-user"></i>By <a
                                            href="javascript:;">demongo</a> </li>
                                    <li class="post-comment"><i class="fa fa-comments"></i> <a href="javascript:;">0
                                            comment</a> </li>
                                </ul>
                            </div>
                            <div class="dez-post-text">
                                <p>Our construction design provides an extraordinary construction project
                                    experience. We are able to provide our customers with a level...</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="blog-post latest-blog-1 date-style-3 skew-date">
                        <div class="dez-post-media dez-img-effect zoom-slow"> <a href="javascript:;"><img
                                    src="images/blog/latest-blog/pic3.jpg" alt=""></a> </div>
                        <div class="dez-post-info p-t10">
                            <div class="dez-post-title ">
                                <h3 class="post-title"><a href="javascript:;">Creative construct
                                        design</a></h3>
                            </div>
                            <div class="dez-post-meta ">
                                <ul>
                                    <li class="post-date"> <i class="fa fa-calendar"></i><strong>10
                                            Aug</strong> <span> 2020</span> </li>
                                    <li class="post-author"><i class="fa fa-user"></i>By <a
                                            href="javascript:;">demongo</a> </li>
                                    <li class="post-comment"><i class="fa fa-comments"></i> <a href="javascript:;">0
                                            comment</a> </li>
                                </ul>
                            </div>
                            <div class="dez-post-text">
                                <p>We understand the importance of the creation and professionalism and work
                                    with the best creative team memeber to achieve this goal...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
