<script>
    // const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    Response = {
        package: (formData) => {
            let plainFormData = Object.fromEntries(formData.entries());
            return plainFormData;
        },
        handleErrors: (response) => {
            if (response.status == 400) {
                let returnErr = response.responseJSON.msg;
                errTemplate = returnErr;
                if (typeof(returnErr) == 'object') {
                    errTemplate = '';
                    for (let prop in returnErr) {
                        errTemplate += `${returnErr[prop]}</br>`
                    }
                }
                Swal.fire(
                    'error',
                    errTemplate,
                    "error"
                );
            } else {
                Swal.fire(
                    '錯誤',
                    '麻煩聯絡系統管理員',
                    'error'
                )
            }
            return;
        },
        post: (requireURL, data = null) => {
            return $.ajax({
                type: 'POST',
                url: requireURL,
                data: data,
                dataType: 'json'
            }).fail((err) => {
                Response.handleErrors(err);
            })
        },
        // post: (requireURL, data = null) => {
        //     fetch(`{{ url('${requireURL}') }}`, {
        //         headers: {
        //             'Content-Type': 'application/json',
        //             "X-CSRF-Token": csrfToken
        //         },
        //         method: 'post',
        //         body: JSON.stringify(data)
        //     }).then((res) => {
        //         return [res.status, res.json()];
        //         // Response.handleErrors(res)
        //     }).then((res) => {
        //         console.log(res);
        //         // Response.handleErrors(res);
        //     })
        // },
        // get: (requireURL) => {
        //     return new Promise((reslove, reject) => {
        //         fetch(`{{ url('${requireURL}') }}`, {
        //             headers: {
        //                 'Content-Type': 'application/json',
        //                 "X-CSRF-Token": csrfToken
        //             },
        //             method: 'get',
        //         }).then((res) => {
        //             Response.handleErrors(res, reslove, reject)
        //         })
        //     });
        // },
    }
</script>
