@extends('Template.template')
@section('header')
    @include('Login.header')
@endsection
@section('body')
    <div id="loading-area"></div>
    <div class="page-wrapers">
        <!-- Content -->
        <div class="page-content dez-login bg-secondry">
            @include('Login.mainComponent.logoHeader')
            @include('Login.mainComponent.loginForm')
            @include('Login.mainComponent.footer')
        </div>
        <!-- Content END-->
    </div>
@endsection
@section('script')
    @include('Login.js')
@endsection
@section('footer')
    @include('Login.footer')
@endsection
