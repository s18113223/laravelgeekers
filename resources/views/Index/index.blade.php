@extends('Template.template')
@section('header')
    @include('Index.header')
@endsection
@section('body')


    <div id="loading-area"></div>
    <div class="page-wraper">
        <!-- header -->
        @include('Basic.header')
        <!-- header END -->
        <!-- Content -->
        <div class="page-content">
            <!-- Slider -->
            @include('Index.mainComponent.slider')
            <!-- Slider END -->
            <!-- meet & ask -->
            @include('Index.mainComponent.meetAndAsk')
            <!-- meet & ask END -->
            <!-- About Company -->
            @include('Index.mainComponent.about')
            <!-- About Company END -->
            <!-- Our Projects  -->
            @include('Index.mainComponent.projects')
            <!-- Our Projects END -->
            <!-- Architecture -->
            @include('Index.mainComponent.architecture')
            <!-- Architecture END-->
            <!-- Company staus -->
            @include('Index.mainComponent.company')
            <!-- Company staus END -->
            <!-- Team member -->
            @include('Index.mainComponent.member')
            <!-- Team member END -->
            <!-- Latest blog -->
            @include('Index.mainComponent.latestBlog')
            <!-- Latest blog END -->
            <!-- Testimonials blog -->
            @include('Index.mainComponent.testimonialsBlog')
            <!-- Testimonials blog END -->
            <!-- Client logo -->
            @include('Index.mainComponent.clientLogo')
            <!-- Client logo END -->
        </div>
        <!-- Content END-->
        <!-- Footer -->
        @include('Basic.footer')
        <!-- Footer END-->
        <!-- scroll top button -->
        @include('Basic.scroltop')

    </div>
@endsection
@section('script')
    @include('Index.js')
@endsection
@section('footer')
    @include('Index.footer')
@endsection
