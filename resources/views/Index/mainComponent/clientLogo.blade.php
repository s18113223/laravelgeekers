<div class="section-full dez-we-find bg-img-fix client-logo-area">
    <div class="container">
        <div class="section-content">
            <div class="client-logo-carousel owl-carousel owl-btn-center-lr">
                <div class="item">
                    <div class="ow-client-logo">
                        <div class="client-logo"><a href="#"><img src="images/client-logo/logo1.jpg" alt=""></a></div>
                    </div>
                </div>
                <div class="item">
                    <div class="ow-client-logo">
                        <div class="client-logo"> <a href="#"><img src="images/client-logo/logo2.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ow-client-logo">
                        <div class="client-logo"> <a href="#"><img src="images/client-logo/logo1.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ow-client-logo">
                        <div class="client-logo"> <a href="#"><img src="images/client-logo/logo3.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ow-client-logo">
                        <div class="client-logo"> <a href="#"><img src="images/client-logo/logo4.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="ow-client-logo">
                        <div class="client-logo"> <a href="#"><img src="images/client-logo/logo3.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
