<div class="section-full bg-img-fix content-inner-2 overlay-black-middle"
    style="background-image:url(images/background/bg1.jpg);">
    <div class="container">
        <div class="section-head  text-center text-white">
            <h2 class="text-uppercase">Our Projects</h2>
            <div class="dez-separator-outer ">
                <div class="dez-separator bg-white style-skew"></div>
            </div>
            <p>Because of best quality & service, victory has always been our goal, we only repRecent the
                best talent. We’ll do everything for you which can put you at ease with the correct
                guidance, simplicity & honesty.</p>
        </div>
        <div class="site-filters clearfix center m-b40" id="portfolio">
            <ul class="filters" data-toggle="buttons">
                <li data-filter="" class="btn active">
                    <input type="radio">
                    <a href="#" class="site-button-secondry button-skew"><span>Show All</span></a>
                </li>
                <li data-filter="home" class="btn">
                    <input type="radio">
                    <a href="#" class="site-button-secondry button-skew"><span>Builder</span></a>
                </li>
                <li data-filter="office" class="btn">
                    <input type="radio">
                    <a href="#" class="site-button-secondry button-skew"><span>Electric</span></a>
                </li>
                <li data-filter="commercial" class="btn">
                    <input type="radio">
                    <a href="#" class="site-button-secondry button-skew"><span>Painting</span></a>
                </li>
                <li data-filter="window" class="btn">
                    <input type="radio">
                    <a href="#" class="site-button-secondry button-skew"><span>Hammer</span></a>
                </li>
            </ul>
        </div>
        <div class="clearfix">
            <ul id="masonry" class="row dez-gallery-listing gallery-grid-4 gallery lightgallery">
                <li class="home card-container col-lg-4 col-md-4 col-sm-6">
                    <div class="dez-box dez-gallery-box">
                        <div class="dez-media dez-img-overlay1 dez-img-effect zoom-slow"> <a href="javascript:void(0);">
                                <img src="images/our-work/pic1.jpg" alt=""> </a>
                            <div class="overlay-bx">
                                <div class="overlay-icon">
                                    <a href="project-details.html"> <i class="fa fa-link icon-bx-xs"></i>
                                    </a>
                                    <span data-exthumbimage="images/our-work/thumb/pic1.jpg"
                                        data-src="images/our-work/large/pic1.jpg" class="icon-bx-xs check-km"
                                        title="Light Gallery Grid 1">
                                        <i class="fa fa-picture-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="office card-container col-lg-4 col-md-4 col-sm-6">
                    <div class="dez-box dez-gallery-box">
                        <div class="dez-media dez-img-overlay1 dez-img-effect zoom-slow dez-img-effect zoom">
                            <a href="javascript:void(0);"> <img src="images/our-work/pic2.jpg" alt=""> </a>
                            <div class="overlay-bx">
                                <div class="overlay-icon">
                                    <a href="project-details.html"> <i class="fa fa-link icon-bx-xs"></i>
                                    </a>
                                    <span data-exthumbimage="images/our-work/thumb/pic2.jpg"
                                        data-src="images/our-work/large/pic2.jpg" class="icon-bx-xs check-km"
                                        title="Light Gallery Grid 1">
                                        <i class="fa fa-picture-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="children-aid card-container col-lg-4 col-md-4 col-sm-6">
                    <div class="dez-box dez-gallery-box">
                        <div class="dez-media dez-img-overlay1 dez-img-effect zoom-slow"> <a href="javascript:void(0);">
                                <img src="images/our-work/pic3.jpg" alt=""> </a>
                            <div class="overlay-bx">
                                <div class="overlay-icon">
                                    <a href="project-details.html"> <i class="fa fa-link icon-bx-xs"></i>
                                    </a>
                                    <span data-exthumbimage="images/our-work/thumb/pic3.jpg"
                                        data-src="images/our-work/large/pic3.jpg" class="icon-bx-xs check-km"
                                        title="Light Gallery Grid 1">
                                        <i class="fa fa-picture-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="commercial card-container col-lg-4 col-md-4 col-sm-6">
                    <div class="dez-box dez-gallery-box">
                        <div class="dez-media dez-img-overlay1 dez-img-effect zoom-slow"> <a href="javascript:void(0);">
                                <img src="images/our-work/pic4.jpg" alt=""> </a>
                            <div class="overlay-bx">
                                <div class="overlay-icon">
                                    <a href="project-details.html"> <i class="fa fa-link icon-bx-xs"></i>
                                    </a>
                                    <span data-exthumbimage="images/our-work/thumb/pic4.jpg"
                                        data-src="images/our-work/large/pic4.jpg" class="icon-bx-xs check-km"
                                        title="Light Gallery Grid 1">
                                        <i class="fa fa-picture-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="window card-container col-lg-4 col-md-4 col-sm-6">
                    <div class="dez-box dez-gallery-box">
                        <div class="dez-media dez-img-overlay1 dez-img-effect zoom-slow"> <a href="javascript:void(0);">
                                <img src="images/our-work/pic5.jpg" alt=""> </a>
                            <div class="overlay-bx">
                                <div class="overlay-icon">
                                    <a href="project-details.html"> <i class="fa fa-link icon-bx-xs"></i>
                                    </a>
                                    <span data-exthumbimage="images/our-work/thumb/pic5.jpg"
                                        data-src="images/our-work/large/pic5.jpg" class="icon-bx-xs check-km"
                                        title="Light Gallery Grid 1">
                                        <i class="fa fa-picture-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="apartment card-container col-lg-4 col-md-4 col-sm-6">
                    <div class="dez-box dez-gallery-box">
                        <div class="dez-media dez-img-overlay1 dez-img-effect zoom-slow"> <a href="javascript:void(0);">
                                <img src="images/our-work/pic6.jpg" alt=""> </a>
                            <div class="overlay-bx">
                                <div class="overlay-icon">
                                    <a href="project-details.html"> <i class="fa fa-link icon-bx-xs"></i>
                                    </a>
                                    <span data-exthumbimage="images/our-work/thumb/pic6.jpg"
                                        data-src="images/our-work/large/pic6.jpg" class="icon-bx-xs check-km"
                                        title="Light Gallery Grid 1">
                                        <i class="fa fa-picture-o"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
