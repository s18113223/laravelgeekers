<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Identity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->session()->has('account')) {
            return redirect('login')->withErrors(['msg' => 'LOGIN NOT YET']);
        }
        return $next($request);
    }
}