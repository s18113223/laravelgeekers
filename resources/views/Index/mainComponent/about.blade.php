 <div class="section-full  bg-gray content-inner-1"
     style="background-image: url(images/bg-img.png); background-repeat: repeat-x; background-position: left bottom -37px;">
     <div class="container">
         <div class="section-content">
             <div class="row">
                 <div class="col-lg-7">
                     <h2 class="text-uppercase"> About Company</h2>
                     <div class="dez-separator-outer ">
                         <div class="dez-separator bg-secondry style-skew"></div>
                     </div>
                     <div class="clear"></div>
                     <p><strong>Constructzilla is ready to build you. We provide best construction service to
                             our clients. We create best building design that you should be proud of. To
                             realize your idea, we work beautifully and creatively for your own
                             dream.</strong></p>
                     <p class="m-b50">We are thinkers and dreamers, maker installation and product
                         for buildings. We build and has built hotels, residences hospitals and sports
                         venues. We are devoted to the task to construct your dream to fit all your needs and
                         preference. We realize that our success starts and ends with our employees so we try
                         to provide excellent work to our clients with a level of expertise. </p>
                     <div class="row">
                         <div class="col-md-6 col-lg-6 col-sm-6">
                             <div class="icon-bx-wraper left m-b30">
                                 <div class="icon-bx-sm bg-secondry "> <span class="icon-cell"><i
                                             class="fa fa-building-o text-primary"></i></span> </div>
                                 <div class="icon-content">
                                     <h3 class="dez-tilte text-uppercase">Construction</h3>
                                     <p>We provide the best construction project for you.</p>
                                 </div>
                             </div>
                         </div>
                         <div class="col-md-6 col-lg-6 col-sm-6">
                             <div class="icon-bx-wraper left m-b30">
                                 <div class="icon-bx-sm bg-secondry "> <span class="icon-cell"><i
                                             class="fa fa-user text-primary"></i></span> </div>
                                 <div class="icon-content">
                                     <h3 class="dez-tilte text-uppercase">Architecture</h3>
                                     <p>Our architect service provides high-end design for you.</p>
                                 </div>
                             </div>
                         </div>
                         <div class="col-md-6 col-lg-6 col-sm-6">
                             <div class="icon-bx-wraper left m-b30">
                                 <div class="icon-bx-sm bg-secondry "> <span class="icon-cell"><i
                                             class="fa fa-truck text-primary"></i></span> </div>
                                 <div class="icon-content">
                                     <h3 class="dez-tilte text-uppercase">Consulting</h3>
                                     <p>Our consulting team is always ready to help you.</p>
                                 </div>
                             </div>
                         </div>
                         <div class="col-md-6 col-lg-6 col-sm-6">
                             <div class="icon-bx-wraper left m-b30">
                                 <div class="icon-bx-sm bg-secondry "> <span class="icon-cell"><i
                                             class="fa fa-book text-primary"></i></span> </div>
                                 <div class="icon-content">
                                     <h3 class="dez-tilte text-uppercase">Mechanical</h3>
                                     <p>We are mechanically strong to build your building. </p>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-5">
                     <div class="dez-thu m"><img src="images/worker.png" alt=""></div>
                 </div>
             </div>
         </div>
     </div>
 </div>
