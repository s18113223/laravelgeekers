<div class="section-full z-index100 meet-ask-outer">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 meet-ask-row p-tb30">
                <div class="row d-flex">
                    <div class="col-lg-6">
                        <div class="icon-bx-wraper clearfix text-white left">
                            <div class="icon-xl "> <span class=" icon-cell"><i
                                        class="fa fa-building-o"></i></span> </div>
                            <div class="icon-content">
                                <h3 class="dez-tilte text-uppercase m-b10">Meet & Ask</h3>
                                <p>You will share your project needs, dreams and goals with us. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 m-t20">
                        <a href="#" class="site-button-secondry button-skew m-l10">
                            <span>Contact Us</span><i class="fa fa-angle-right"></i></a>
                        <a href="#" class="site-button-secondry button-skew m-l20">
                            <span>View more</span><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
