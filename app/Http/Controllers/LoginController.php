<?php

namespace App\Http\Controllers;

use App\Models\Login;
use Illuminate\Http\Request;
use Validator;
class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->session()->has('account')) {
            return redirect('contact');
        } else {
            return view('Login.index');
        }
    }

   public function login(Request $request)
   {
        $data = $request->all();
        $rules = [
            'account' => 'required',
            'password' => 'required',
        ];
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $result =  ['status' => self::$LOGIN_ERROR, 'msg' => $validator->errors()];
            return response()->json($result,400);
        } else {
            $isUser = $this->checkLogin($data['account'], $data['password']);
            if ($isUser == true) {
                $request->session()->put('account', $data['account']);
                $request->session()->put('name', self::$NAME);
                $result = ['status' => self::$LOGIN_SUCCESS, 'msg' => '登入成功'];
                return response()->json($result,200);
            } else {
                $result = ['status' => self::$LOGIN_UNKNOWN_ERROR, 'msg' => '登入失敗'];
                return response()->json($result,400);
            }
        }
   }
   public function checkLogin($account, $password)
   {
       if($account == self::$ACCOUNT && $password == self::$PASSWORD){
           return true;
       }
   }
   public function out(Request $request)
   {
        $request->session()->flush(); 
        return redirect('/');    
   }
}