  <div class="main-slider style-two default-banner">
      <div class="tp-banner-container">
          <div class="tp-banner">
              <div id="rev_slider_1014_1_wrapper" class="rev_slider_wrapper fullscreen-container"
                  data-alias="typewriter-effect" data-source="gallery">
                  <!-- START REVOLUTION SLIDER 5.3.0.2 -->
                  <div id="rev_slider_1014_1" class="rev_slider fullscreenbanner" style="display:none;"
                      data-version="5.3.0.2">
                      <ul>
                          <!-- SLIDE 1 -->
                          <li data-index="rs-1000" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                              data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default"
                              data-easeout="default" data-masterspeed="default"
                              data-thumb="images/main-slider/slide1.jpg" data-rotate="0" data-saveperformance="off"
                              data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4=""
                              data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10=""
                              data-description="">
                              <!-- MAIN IMAGE -->
                              <img src="images/main-slider/slide1.jpg" alt="" data-bgposition="center center"
                                  data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina />
                              <!-- LAYERS -->
                              <!-- LAYER NR. 1 [ for overlay ] -->
                              <div class="tp-caption tp-shape tp-shapewrapper " id="slide-100-layer-1"
                                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                  data-width="full" data-height="full" data-whitespace="nowrap" data-type="shape"
                                  data-basealign="slide" data-responsive_offset="off" data-responsive="off"
                                  data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}]'
                                  data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                  data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                  data-paddingleft="[0,0,0,0]"
                                  style="z-index: 12;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0);border-width:0px;">
                              </div>

                              <!-- LAYER NR. 2 [ for title ] -->
                              <div class="tp-caption  tp-resizeme" id="slide-100-layer-2"
                                  data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                  data-y="['top','top','top','top']" data-voffset="['170','150','150','140']"
                                  data-fontsize="['70','60','50','40']" data-lineheight="['70','60','50','40']"
                                  data-width="['700','700','700','700']" data-height="['none','none','none','none']"
                                  data-whitespace="['normal','normal','normal','normal']" data-type="text"
                                  data-responsive_offset="on"
                                  data-frames='[{"from":"y:50px(R);opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}]'
                                  data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                  data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                  data-paddingleft="[0,0,0,0]"
                                  style="z-index: 13; white-space: normal; font-size: 60px; line-height: 60px; font-weight: 700; color: rgba(255, 255, 255, 1.00); border-width:0px;">
                                  <span class="text-uppercase" style="font-family: Oswald;">We build your
                                      Dream</span>
                              </div>

                              <!-- LAYER NR. 3 [ for paragraph] -->
                              <div class="tp-caption tp-resizeme" id="slide-100-layer-3"
                                  data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                  data-y="['top','top','top','top']" data-voffset="['260','250','225','200']"
                                  data-fontsize="['18','18','18','16']" data-lineheight="['30','30','30','24']"
                                  data-width="['800','800','600','400']" data-height="['none','none','none','none']"
                                  data-whitespace="['normal','normal','normal','normal']" data-type="text"
                                  data-responsive_offset="on" data-frames='[
                                          {"from":"y:50px(R);opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                          {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                          ]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                  data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                  data-paddingleft="[0,0,0,0]"
                                  style="z-index: 13; font-weight: 500; color: rgba(255, 255, 255, 0.85); border-width:0px;">
                                  <span>With a strong sense of the value of diversity, we create unique solid
                                      construct design and other exclusive solutions with architect. We are
                                      thinkers & dreamers, makers, creating lighting, installations & products
                                      for homes, education, hotels & more.</span>
                              </div>

                              <!-- LAYER NR. 4 [ for readmore botton ] -->
                              <div class="tp-caption tp-resizeme" id="slide-100-layer-4"
                                  data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                  data-y="['top','top','top','top']" data-voffset="['370','370','370','340']"
                                  data-fontsize="['none','none','none','none']"
                                  data-lineheight="['none','none','none','none']" data-width="['700','700','700','700']"
                                  data-height="['none','none','none','none']"
                                  data-whitespace="['normal','normal','normal','normal']" data-type="text"
                                  data-responsive_offset="on" data-frames='[ 
                                          {"from":"y:50px(R);opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                          {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                          ]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                  data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                  data-paddingleft="[0,0,0,0]" style="z-index: 13;">
                                  <a href="javascript:void(0);" class="site-button  button-skew">
                                      <span>Read More</span><i class="fa fa-angle-right"></i>
                                  </a>
                              </div>
                          </li>
                          <!-- SLIDE 2 -->
                          <li data-index="rs-2000" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                              data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default"
                              data-easeout="default" data-masterspeed="default"
                              data-thumb="images/main-slider/slide2.jpg" data-rotate="0" data-saveperformance="off"
                              data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4=""
                              data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10=""
                              data-description="">
                              <!-- MAIN IMAGE -->
                              <img src="images/main-slider/slide2.jpg" alt="" data-bgposition="center center"
                                  data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina />
                              <!-- LAYERS -->
                              <!-- LAYER NR. 1 [ for overlay ] -->
                              <div class="tp-caption tp-shape tp-shapewrapper " id="slide-200-layer-1"
                                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                  data-width="full" data-height="full" data-whitespace="nowrap" data-type="shape"
                                  data-basealign="slide" data-responsive_offset="off" data-responsive="off"
                                  data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}]'
                                  data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                  data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                  data-paddingleft="[0,0,0,0]"
                                  style="z-index: 12;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0);border-width:0px;">
                              </div>
                              <!-- LAYER NR. 2 [ for title ] -->
                              <div class="tp-caption  tp-resizeme" id="slide-200-layer-2"
                                  data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                  data-y="['top','top','top','top']" data-voffset="['170','150','150','140']"
                                  data-fontsize="['70','60','50','40']" data-lineheight="['70','60','50','40']"
                                  data-width="['700','700','700','700']" data-height="['none','none','none','none']"
                                  data-whitespace="['normal','normal','normal','normal']" data-type="text"
                                  data-responsive_offset="on"
                                  data-frames='[{"from":"y:50px(R);opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}]'
                                  data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                  data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                  data-paddingleft="[0,0,0,0]"
                                  style="z-index: 13; white-space: normal; font-size: 60px; line-height: 60px; font-weight: 700; color: rgba(255, 255, 255, 1.00); border-width:0px;">
                                  <span class="text-uppercase" style="font-family: Oswald;">We build your
                                      Dream</span>
                              </div>
                              <!-- LAYER NR. 3 [ for paragraph] -->
                              <div class="tp-caption tp-resizeme" id="slide-200-layer-3"
                                  data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                  data-y="['top','top','top','top']" data-voffset="['260','250','225','200']"
                                  data-fontsize="['18','18','18','16']" data-lineheight="['30','30','30','24']"
                                  data-width="['800','800','600','400']" data-height="['none','none','none','none']"
                                  data-whitespace="['normal','normal','normal','normal']" data-type="text"
                                  data-responsive_offset="on" data-frames='[
                                          {"from":"y:50px(R);opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                          {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                          ]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                  data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                  data-paddingleft="[0,0,0,0]"
                                  style="z-index: 13; font-weight: 500; color: rgba(255, 255, 255, 0.85); border-width:0px;">
                                  <span>With a strong sense of the value of diversity, we create unique solid
                                      construct design and other exclusive solutions with architect. We are
                                      thinkers & dreamers, makers, creating lighting, installations & products
                                      for homes, education, hotels & more.</span>
                              </div>
                              <!-- LAYER NR. 4 [ for readmore botton ] -->
                              <div class="tp-caption tp-resizeme" id="slide-200-layer-4"
                                  data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                  data-y="['top','top','top','top']" data-voffset="['370','370','370','340']"
                                  data-fontsize="['none','none','none','none']"
                                  data-lineheight="['none','none','none','none']" data-width="['700','700','700','700']"
                                  data-height="['none','none','none','none']"
                                  data-whitespace="['normal','normal','normal','normal']" data-type="text"
                                  data-responsive_offset="on" data-frames='[ 
                                          {"from":"y:50px(R);opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                          {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                          ]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                  data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                  data-paddingleft="[0,0,0,0]" style="z-index: 13;">
                                  <a href="javascript:void(0);" class="site-button  button-skew">
                                      <span>Read More</span><i class="fa fa-angle-right"></i>
                                  </a>
                              </div>
                          </li>
                          <!-- SLIDE 3 -->
                          <li data-index="rs-3000" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                              data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default"
                              data-easeout="default" data-masterspeed="default"
                              data-thumb="images/main-slider/slide3.jpg" data-rotate="0" data-saveperformance="off"
                              data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4=""
                              data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10=""
                              data-description="">
                              <!-- MAIN IMAGE -->
                              <img src="images/main-slider/slide3.jpg" alt="" data-bgposition="center center"
                                  data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina />
                              <!-- LAYERS -->
                              <!-- LAYER NR. 1 [ for overlay ] -->
                              <div class="tp-caption tp-shape tp-shapewrapper " id="slide-300-layer-1"
                                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                  data-width="full" data-height="full" data-whitespace="nowrap" data-type="shape"
                                  data-basealign="slide" data-responsive_offset="off" data-responsive="off"
                                  data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}]'
                                  data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                  data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                  data-paddingleft="[0,0,0,0]"
                                  style="z-index: 12;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0);border-width:0px;">
                              </div>
                              <!-- LAYER NR. 2 [ for title ] -->
                              <div class="tp-caption   tp-resizeme" id="slide-300-layer-2"
                                  data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                  data-y="['top','top','top','top']" data-voffset="['170','150','150','140']"
                                  data-fontsize="['70','60','50','40']" data-lineheight="['70','60','50','40']"
                                  data-width="['700','700','700','700']" data-height="['none','none','none','none']"
                                  data-whitespace="['normal','normal','normal','normal']" data-type="text"
                                  data-responsive_offset="on"
                                  data-frames='[{"from":"y:50px(R);opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}]'
                                  data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                  data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                  data-paddingleft="[0,0,0,0]"
                                  style="z-index: 13; white-space: normal; font-size: 60px; line-height: 60px; font-weight: 700; color: rgba(255, 255, 255, 1.00); border-width:0px;">
                                  <span class="text-uppercase" style="font-family: Oswald;">We build your
                                      Dream</span>
                              </div>
                              <!-- LAYER NR. 3 [ for paragraph] -->
                              <div class="tp-caption  tp-resizeme" id="slide-300-layer-3"
                                  data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                  data-y="['top','top','top','top']" data-voffset="['260','250','225','200']"
                                  data-fontsize="['18','18','18','16']" data-lineheight="['30','30','30','24']"
                                  data-width="['800','800','600','400']" data-height="['none','none','none','none']"
                                  data-whitespace="['normal','normal','normal','normal']" data-type="text"
                                  data-responsive_offset="on" data-frames='[
                                          {"from":"y:50px(R);opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                          {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                          ]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                  data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                  data-paddingleft="[0,0,0,0]" style="z-index: 13; 
                                          font-weight: 500; 
                                          color: rgba(255, 255, 255, 0.85);
                                          border-width:0px;"> <span>With a strong sense of the value of diversity, we
                                      create
                                      unique
                                      solid
                                      construct
                                      design and other exclusive solutions with architect. We are thinkers &
                                      dreamers, makers, creating lighting, installations & products for homes,
                                      education, hotels & more.</span>
                              </div>
                              <!-- LAYER NR. 4 [ for readmore botton ] -->
                              <div class="tp-caption tp-resizeme" id="slide-300-layer-4"
                                  data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                                  data-y="['top','top','top','top']" data-voffset="['370','370','370','340']"
                                  data-fontsize="['none','none','none','none']"
                                  data-lineheight="['none','none','none','none']" data-width="['700','700','700','700']"
                                  data-height="['none','none','none','none']"
                                  data-whitespace="['normal','normal','normal','normal']" data-type="text"
                                  data-responsive_offset="on" data-frames='[ 
                                          {"from":"y:50px(R);opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                          {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                          ]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                  data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                                  data-paddingleft="[0,0,0,0]" style="z-index: 13;">
                                  <a href="javascript:void(0);" class="site-button  button-skew">
                                      <span>Read More</span><i class="fa fa-angle-right"></i>
                                  </a> cxz
                              </div>
                          </li>
                      </ul>
                      <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                  </div>
              </div>
              <!-- END REVOLUTION SLIDER -->
          </div>
      </div>
  </div>
