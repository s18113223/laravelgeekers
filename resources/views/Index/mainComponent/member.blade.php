<div class="section-full bg-white content-inner abc">
    <div class="container">
        <div class="section-head text-center ">
            <h2 class="text-uppercase"> Meet The Team</h2>
            <div class="dez-separator-outer ">
                <div class="dez-separator bg-secondry style-skew"></div>
            </div>
            <p>Our smart team takes care of everything. The entire team has been great to work with from
                start to finish. Our team is focused on target and best service. </p>
        </div>
        <div class="section-content text-center ">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="dez-box m-b30">
                        <div class="dez-media"> <a href="javascript:;"> <img width="358" height="460" alt=""
                                    src="images/our-team/pic1.jpg"> </a>
                            <div class="dez-info-has skew-has  bg-primary">
                                <ul class="dez-social-icon dez-border">
                                    <li><a class="fa fa-facebook" href="javascript:void(0);"></a></li>
                                    <li><a class="fa fa-twitter" href="javascript:void(0);"></a></li>
                                    <li><a class="fa fa-linkedin" href="javascript:void(0);"></a></li>
                                    <li><a class="fa fa-facebook" href="javascript:void(0);"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="p-a15">
                            <h4 class="dez-title m-b5 text-capitalize"><a href="javascript:;">Nashid
                                    Martines</a></h4>
                            <span class="dez-member-position">Director</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="dez-box m-b30">
                        <div class="dez-media"> <a href="javascript:;"> <img width="358" height="460" alt=""
                                    src="images/our-team/pic2.jpg"> </a>
                            <div class="dez-info-has skew-has bg-primary">
                                <ul class="dez-social-icon dez-border">
                                    <li><a class="fa fa-facebook" href="javascript:void(0);"></a></li>
                                    <li><a class="fa fa-twitter" href="javascript:void(0);"></a></li>
                                    <li><a class="fa fa-linkedin" href="javascript:void(0);"></a></li>
                                    <li><a class="fa fa-facebook" href="javascript:void(0);"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="p-a15">
                            <h4 class="dez-title m-b5 text-capitalize"><a href="javascript:;">Hackson
                                    Willingham</a></h4>
                            <span class="dez-member-position">Developer</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="dez-box m-b30">
                        <div class="dez-media"> <a href="javascript:;"> <img width="358" height="460" alt=""
                                    src="images/our-team/pic3.jpg"> </a>
                            <div class="dez-info-has skew-has bg-primary">
                                <ul class="dez-social-icon dez-border">
                                    <li><a class="fa fa-facebook" href="javascript:void(0);"></a></li>
                                    <li><a class="fa fa-twitter" href="javascript:void(0);"></a></li>
                                    <li><a class="fa fa-linkedin" href="javascript:void(0);"></a></li>
                                    <li><a class="fa fa-facebook" href="javascript:void(0);"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="p-a15 bg-white">
                            <h4 class="dez-title m-b5 text-capitalize"><a href="javascript:;">konne
                                    Backfield</a></h4>
                            <span class="dez-member-position">Designer</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="dez-box m-b10">
                        <div class="dez-media"> <a href="javascript:;"> <img width="358" height="460" alt=""
                                    src="images/our-team/pic4.jpg"> </a>
                            <div class="dez-info-has skew-has bg-primary">
                                <ul class="dez-social-icon dez-border">
                                    <li><a class="fa fa-facebook" href="javascript:void(0);"></a></li>
                                    <li><a class="fa fa-twitter" href="javascript:void(0);"></a></li>
                                    <li><a class="fa fa-linkedin" href="javascript:void(0);"></a></li>
                                    <li><a class="fa fa-facebook" href="javascript:void(0);"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="p-a15">
                            <h4 class="dez-title m-b5 text-capitalize"><a href="javascript:;">konne
                                    Backfield</a></h4>
                            <span class="dez-member-position">Manager</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
