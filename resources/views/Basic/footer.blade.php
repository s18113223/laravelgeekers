<footer class="site-footer">
    <!-- newsletter part -->
    <div class="bg-primary dez-newsletter">
        <div class="container equal-wraper">
            <form class="dzSubscribe" action="script/mailchamp.php" method="post">
                <div class="row">
                    <div class="col-md-4 col-lg-4">
                        <div class="icon-bx-wraper equal-col p-t30 p-b20 left">
                            <div class="icon-lg text-primary radius"> <a href="#" class="icon-cell"><i
                                        class="fa fa-envelope-o"></i></a> </div>
                            <div class="icon-content"> <strong
                                    class="text-black text-uppercase font-18">Subscribe</strong>
                                <h2 class="dez-tilte text-uppercase">Our Newsletter</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="dzSubscribeMsg"></div>
                        <div class="input-group equal-col p-t40 p-b20">
                            <input name="dzEmail" required placeholder="Email address" required="required"
                                class="form-control" type="email">
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 offset-lg-1 offset-md-1">
                        <div class="equal-col p-t40 p-b20 skew-subscribe">
                            <button name="submit" value="Submit" type="submit"
                                class="site-button-secondry button-skew z-index1">
                                <span>Subscribe</span><i class="fa fa-angle-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- footer top part -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 footer-col-4">
                    <div class="widget widget_about">
                        <div class="logo-footer"><img src="images/logo-dark.png" alt=""></div>
                        <p><strong>Our mission </strong>is to provide the best value and service to our clients.
                            Here we are always ready to help you. We are devoted to the task to construct your dream
                            to fit all your needs and preference. We realize
                            that our success starts and ends with our employees.</p>
                        <ul class="dez-social-icon dez-border">
                            <li>
                                <a class="fa fa-facebook" href="javascript:void(0);"></a>
                            </li>
                            <li>
                                <a class="fa fa-twitter" href="javascript:void(0);"></a>
                            </li>
                            <li>
                                <a class="fa fa-linkedin" href="javascript:void(0);"></a>
                            </li>
                            <li>
                                <a class="fa fa-facebook" href="javascript:void(0);"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 footer-col-4">
                    <div class="widget recent-posts-entry">
                        <h4 class="m-b15 text-uppercase">Recent Post</h4>
                        <div class="dez-separator-outer m-b10">
                            <div class="dez-separator bg-white style-skew"></div>
                        </div>
                        <div class="widget-post-bx">
                            <div class="widget-post clearfix">
                                <div class="dez-post-media"> <img src="images/blog/recent-blog/pic1.jpg" alt=""
                                        width="200" height="143"> </div>
                                <div class="dez-post-info">
                                    <div class="dez-post-header">
                                        <h6 class="post-title text-uppercase"><a href="blog-single.html">Title of
                                                first blog</a></h6>
                                    </div>
                                    <div class="dez-post-meta">
                                        <ul>
                                            <li class="post-author">By <a href="#">Admin</a></li>
                                            <li class="post-comment"><i class="fa fa-comments"></i> 28</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-post clearfix">
                                <div class="dez-post-media"> <img src="images/blog/recent-blog/pic2.jpg" alt=""
                                        width="200" height="160"> </div>
                                <div class="dez-post-info">
                                    <div class="dez-post-header">
                                        <h6 class="post-title text-uppercase"><a href="blog-single.html">Title of
                                                first blog</a></h6>
                                    </div>
                                    <div class="dez-post-meta">
                                        <ul>
                                            <li class="post-author">By <a href="#">Admin</a></li>
                                            <li class="post-comment"><i class="fa fa-comments"></i> 28</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-post clearfix">
                                <div class="dez-post-media"> <img src="images/blog/recent-blog/pic3.jpg" alt=""
                                        width="200" height="160"> </div>
                                <div class="dez-post-info">
                                    <div class="dez-post-header">
                                        <h6 class="post-title  text-uppercase"><a href="blog-single.html">Title of
                                                first blog</a></h6>
                                    </div>
                                    <div class="dez-post-meta">
                                        <ul>
                                            <li class="post-author">By <a href="#">Admin</a></li>
                                            <li class="post-comment"><i class="fa fa-comments"></i> 28</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 footer-col-4">
                    <div class="widget widget_services">
                        <h4 class="m-b15 text-uppercase">Our services</h4>
                        <div class="dez-separator-outer m-b10">
                            <div class="dez-separator bg-white style-skew"></div>
                        </div>
                        <ul>
                            <li><a href="services-2.html">Residential Construction</a></li>
                            <li><a href="services-2.html">Office Construction</a></li>
                            <li><a href="services-2.html">Wall Painting</a></li>
                            <li><a href="services-2.html">Window Construction</a></li>
                            <li><a href="services-2.html">Commercial Construction</a></li>
                            <li><a href="services-2.html">Office Construction</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 footer-col-4">
                    <div class="widget widget_getintuch">
                        <h4 class="m-b15 text-uppercase">Contact us</h4>
                        <div class="dez-separator-outer m-b10">
                            <div class="dez-separator bg-white style-skew"></div>
                        </div>
                        <ul>
                            <li><i class="fa fa-map-marker"></i><strong>address</strong> demo address #8901 Marmora
                                Road Chi Minh City, Vietnam </li>
                            <li><i class="fa fa-phone"></i><strong>phone</strong>0800-123456 (24/7 Support Line)
                            </li>
                            <li><i class="fa fa-fax"></i><strong>FAX</strong>(123) 123-4567</li>
                            <li><i class="fa fa-envelope"></i><strong>email</strong>info@demo.com</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer bottom part -->
    <div class="footer-bottom footer-line">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 text-left">
                    <span>© Copyright 2020</span>
                </div>
                <div class="col-lg-4 col-md-4 text-center">
                    <span> Design With <i class="fa fa-heart text-primary heart"></i> By DexignZone </span>
                </div>
                <div class="col-lg-4 col-md-4 text-right">
                    <a href="about-1.html"> About Us</a>
                    <a href="faq-1.html"> FAQs</a>
                    <a href="contact.html"> Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</footer>
