<div class="col-lg-4 d-lg-flex">
    <div class="p-a30 m-b30 border-1 contact-area align-self-stretch">
        <h2 class="m-b10">Quick Contact</h2>
        <p>If you have any questions simply use the following contact details.</p>
        <ul class="no-margin">
            <li class="icon-bx-wraper left m-b30">
                <div class="icon-bx-xs bg-primary"> <a href="#" class="icon-cell"><i class="fa fa-map-marker"></i></a>
                </div>
                <div class="icon-content">
                    <h6 class="text-uppercase m-tb0 dez-tilte">Address:</h6>
                    <p>123 West Street, Melbourne Victoria 3000 Australia</p>
                </div>
            </li>
            <li class="icon-bx-wraper left  m-b30">
                <div class="icon-bx-xs bg-primary"> <a href="#" class="icon-cell"><i
                            class="fa fa-envelope"></i></a> </div>
                <div class="icon-content">
                    <h6 class="text-uppercase m-tb0 dez-tilte">Email:</h6>
                    <p>info@company.com</p>
                </div>
            </li>
            <li class="icon-bx-wraper left">
                <div class="icon-bx-xs bg-primary"> <a href="#" class="icon-cell"><i
                            class="fa fa-phone"></i></a> </div>
                <div class="icon-content">
                    <h6 class="text-uppercase m-tb0 dez-tilte">PHONE</h6>
                    <p>+61 3 8376 6284</p>
                </div>
            </li>
        </ul>
        <div class="m-t20">
            <ul class="dez-social-icon dez-social-icon-lg">
                <li>
                    <a href="javascript:void(0);" class="fa fa-facebook bg-primary"></a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="fa fa-twitter bg-primary"></a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="fa fa-linkedin bg-primary"></a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="fa fa-pinterest-p bg-primary"></a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="fa fa-google-plus bg-primary"></a>
                </li>
            </ul>
        </div>
    </div>
</div>
