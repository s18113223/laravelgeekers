@extends('Template.template')
@section('header')
    @include('Contact.header')
@endsection
@section('body')
    <div id="loading-area"></div>
    <div class="page-wraper">
        <!-- header -->
        @include('Basic.header')
        <!-- header END -->
        <!-- Content -->
        <div class="page-content">
            <!-- inner page banner -->
            @include('Contact.mainComponent.pageBanner')
            <!-- inner page banner END -->
            <!-- Breadcrumb row -->
            @include('Contact.mainComponent.breadcrumb')
            <!-- Breadcrumb row END -->
            <!-- contact area -->
            <div class="section-full content-inner bg-white contact-style-1">
                <div class="container">
                    <div class="row">
                        <!-- Left part start -->
                        @include('Contact.mainComponent.leftPart')
                        <!-- Left part END -->
                        <!-- right part start -->
                        @include('Contact.mainComponent.rightPart')
                        <!-- right part END -->
                    </div>
                    @include('Contact.mainComponent.map')
                </div>
            </div>
            <!-- contact area  END -->
        </div>
        <!-- Content END-->
        <!-- Footer -->
        @include('Basic.footer')
        <!-- Footer END-->
        <!-- scroll top button -->
        @include('Basic.scroltop')
    </div>
@endsection
@section('script')
    @include('Contact.js')
@endsection
@section('footer')
    @include('Contact.footer')
@endsection
