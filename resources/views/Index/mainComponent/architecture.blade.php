  <div class="section-full  bg-white content-inner overlay-white-middle"
      style="background-image:url(images/background/bg5.png); background-position:right center; background-repeat:no-repeat; background-size: auto 100%;">
      <div class="container">
          <div class="section-head text-center">
              <h2 class="text-uppercase"> Architecture</h2>
              <div class="dez-separator-outer ">
                  <div class="dez-separator bg-secondry style-skew"></div>
              </div>
              <p>We provide a wide variety of services including practical studies, architectural programming
                  and project management. Definitely our work feel amazes you. You will find smoothness and
                  accuracy in our working system.</p>
          </div>
          <div class="row section-content">
              <div class="col-lg-4 col-md-6 col-sm-6">
                  <div class="icon-bx-wraper center m-b40 m-lr15">
                      <div class="icon-bx-sm bg-secondry m-b20"> <span class="icon-cell"><i
                                  class="fa fa-life-saver text-primary"></i></span> </div>
                      <div class="icon-content">
                          <h5 class="dez-tilte text-uppercase">Safety</h5>
                          <p>With the best quality and service we still concentrate on safety and protection.
                          </p>
                      </div>
                  </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-6">
                  <div class="icon-bx-wraper center m-b40 m-lr15">
                      <div class="icon-bx-sm bg-secondry m-b20"> <span class="icon-cell"><i
                                  class="fa fa-users text-primary"></i></span> </div>
                      <div class="icon-content">
                          <h5 class="dez-tilte text-uppercase">Community</h5>
                          <p>We will work and discuss on project with our best team members to define... </p>
                      </div>
                  </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-6">
                  <div class="icon-bx-wraper center m-b40 m-lr15">
                      <div class="icon-bx-sm bg-secondry m-b20"> <span class="icon-cell"><i
                                  class="fa fa-thumbs-up text-primary"></i></span> </div>
                      <div class="icon-content">
                          <h5 class="dez-tilte text-uppercase">Sustainability</h5>
                          <p>We provide an extraordinary construction project for your dream and desires...
                          </p>
                      </div>
                  </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-6">
                  <div class="icon-bx-wraper center m-b40 m-lr15">
                      <div class="icon-bx-sm bg-secondry m-b20"> <span class="icon-cell"><i
                                  class="fa fa-trophy text-primary"></i></span> </div>
                      <div class="icon-content">
                          <h5 class="dez-tilte text-uppercase">Best Quality</h5>
                          <p>We believe in best quality over quantity. We try always best for our clients
                              to... </p>
                      </div>
                  </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-6">
                  <div class="icon-bx-wraper center m-b40 m-lr15">
                      <div class="icon-bx-sm bg-secondry m-b20"> <span class="icon-cell"><i
                                  class="fa fa-cubes text-primary"></i></span> </div>
                      <div class="icon-content">
                          <h5 class="dez-tilte text-uppercase">Integrity</h5>
                          <p>We first create the highest level of trust and integrity with our clients and...
                          </p>
                      </div>
                  </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-6">
                  <div class="icon-bx-wraper center m-b10 m-lr15">
                      <div class="icon-bx-sm bg-secondry m-b20"> <span class="icon-cell"><i
                                  class="fa fa-area-chart text-primary"></i></span> </div>
                      <div class="icon-content">
                          <h5 class="dez-tilte text-uppercase">Strategy</h5>
                          <p>We make our master plan by keeping the needs of the people, which is always...
                          </p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
