<div class="section-full text-white bg-img-fix content-inner overlay-black-middle"
    style="background-image:url(images/background/bg4.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="dex-box text-primary border-2 counter-box m-b30">
                    <h2 class="text-uppercase m-a0 p-a15 "><i class="fa fa-building-o m-r20"></i> <span
                            class="counter">1035</span></h2>
                    <h5 class="dez-tilte  text-uppercase m-a0"><span
                            class="dez-tilte-inner skew-title bg-primary p-lr15 p-tb10">Active
                            Experts</span></h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="dex-box text-primary border-2 counter-box m-b30">
                    <h2 class="text-uppercase m-a0 p-a15 "><i class="fa fa-group m-r20"></i> <span
                            class="counter">1226</span></h2>
                    <h5 class="dez-tilte  text-uppercase m-a0"><span
                            class="dez-tilte-inner skew-title bg-primary p-lr15 p-tb10">Happy Client</span>
                    </h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="dex-box text-primary border-2 counter-box m-b30">
                    <h2 class="text-uppercase m-a0 p-a15 "><i class="fa fa-slideshare m-r20"></i> <span
                            class="counter">1552</span></h2>
                    <h5 class="dez-tilte  text-uppercase m-a0"><span
                            class="dez-tilte-inner skew-title bg-primary p-lr15 p-tb10">Developer
                            Hand</span></h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="dex-box text-primary border-2 counter-box m-b10">
                    <h2 class="text-uppercase m-a0 p-a15 "><i class="fa fa-home m-r20"></i> <span
                            class="counter">1156</span></h2>
                    <h5 class="dez-tilte  text-uppercase m-a0"><span
                            class="dez-tilte-inner skew-title bg-primary p-lr15 p-tb10">Completed
                            Project</span></h5>
                </div>
            </div>
        </div>
    </div>
</div>
