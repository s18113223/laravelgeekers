    <!-- JavaScript  files ========================================= -->
    <script src="js/jquery.min.js"></script>
    <!-- JQUERY.MIN JS -->
    <script src="plugins/bootstrap/js/popper.min.js"></script>
    <!-- BOOTSTRAP.MIN JS -->
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- BOOTSTRAP.MIN JS -->
    <script src="plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <!-- FORM JS -->
    <script src="plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
    <!-- FORM JS -->
    <script src="plugins/magnific-popup/magnific-popup.js"></script>
    <!-- MAGNIFIC POPUP JS -->
    <script src="plugins/counter/waypoints-min.js"></script>
    <!-- WAYPOINTS JS -->
    <script src="plugins/counter/counterup.min.js"></script>
    <!-- COUNTERUP JS -->
    <script src="plugins/imagesloaded/imagesloaded.js"></script>
    <!-- IMAGESLOADED -->
    <script src="plugins/masonry/masonry-3.1.4.js"></script>
    <!-- MASONRY -->
    <script src="plugins/masonry/masonry.filter.js"></script>
    <!-- MASONRY -->
    <script src="plugins/owl-carousel/owl.carousel.js"></script>
    <!-- OWL SLIDER -->
    <script src="js/custom.min.js"></script>
    <!-- CUSTOM FUCTIONS  -->
    <script src="js/dz.carousel.min.js"></script>
    <!-- SORTCODE FUCTIONS  -->
    <script src="js/dz.ajax.js"></script>
    <!-- CONTACT JS  -->
    <script src="js/sweetalert2.min.js"></script>
    @include('Basic.javascript')
    <script>
        var submit = document.querySelector('#btn-login');
        var form = document.querySelector("#dez-form");
        submit.addEventListener("click", (e) => {
            e.preventDefault();
            let data = new FormData(form);
            let newData = Response.package(data);
            console.log(newData);
            Response.post('login', newData)
                .done((res) => {
                    Swal.fire(
                        res.msg,
                        res.msg,
                        'success'
                    ).then((result) => {
                        window.location.href = 'contact';
                    })
                })

        });
    </script>
